from math import log, sqrt, exp, pi

def pseudorandom():
    pd, kd = 5167, 3729
    for _ in range(1000000):
        yield pd * kd // 100 % 10000 / 10000
        kd = pd * kd % 10000

pseudorandom_generator = pseudorandom()

def gener():
    return pseudorandom_generator.__next__()

def ex(m):
    return m * log(1/(1-gener()))

def np(m, s):
    for _ in range(1000):
        b = m + s*sqrt(2*log(100000/(sqrt(2*pi)*s)))
        a = m - s*sqrt(2*log(100000/(sqrt(2*pi)*s)))
        ks1 = gener()
        ks2 = gener()
        dz = (b-a)*ks1+a
        ni = ks2/(sqrt(2*pi)*s)
        if ( (1/(sqrt(2*pi)*s))*exp(-((dz-m)**2)/(2*(s**2))) >= ni ):
            return dz

def tt(problist):
    total = 0
    cumulative = [0]
    for p in problist:
        total = total + p
        cumulative.append(total)
    y = gener()
    for i in range(1,len(cumulative)):
        if y>=cumulative[i-1] and y<cumulative[i]:
            return i


def valmin(m):
    return "{:02d}".format(m // 60) + ":" + "{:02d}".format(m % 60)

def printlog(e):
    timetype = valmin(e["tm"]) + " - " + str(e["tp"]) + " - "
    textline = timetype + e["msg"].format(*e["data"])
    print(textline)
    return None

def pildoBIG(bg, ev):
    if (len(bg) == 0):
        return [ev]
    for i in (range(len(bg))):
        if ev["tm"] < bg[i]["tm"] or (ev["tm"] == bg[i]["tm"] and ev["tp"] == 5): 
            break
    if (ev["tm"] > bg[i]["tm"]) or (ev["tm"] == bg[i]["tm"] and ev["tp"] == 2):
        i = i + 1
    return bg[:i] + [ev] + bg[i:] 

def pildoLKG(lk,l,k):
    if (len(lk) == 0):
        return [k]
    for i, kin in enumerate(lk):
        if l[k-1] < l[kin-1]: 
            break
    if l[k-1] >= l[kin-1]:
        i = i + 1
    return lk[:i] + [k] + lk[i:]



prad = 7
pamilg = 8
pamilg = (pamilg+prad)*60

pa = [0.71, 0.23, 0.05, 0.01]
pr = [0.5, 0.4, 0.1]
ma = [12, 15, 25]
sa = [0.8, 1.0, 3.0]

maxkr = 4
maxkl = 4
N = 0
tau = 5
maxe = 8
tk = [0] * maxkr

BIG = []
ELG = []
LKG = list(range(1, maxkr+1))
#print(tk, LKG)

k = 0
time = prad*60
nk = 0
N = 0
Napt = 0
Ns = 0
Te = 0

BIG = [] 
ev = {"tm": time, "tp": 1, "data": (1,), "msg": "PAMAINA {0} PRASIDEJO"}
BIG = pildoBIG(BIG, ev)
ev = {"tm": pamilg, "tp": 8, "data": (1,), "msg": "OFICIALI PAMAINOS {0} PABAIGA"}
BIG = pildoBIG(BIG, ev)

t = time + int(round(ex(tau)))
ev = {"tm": t, "tp": 2, "data": (0,0), "msg": "Atejo {0} kl.Bendras atejusiu skaicius:{1}."}
BIG = pildoBIG(BIG, ev)

while BIG:
    #print(BIG)
    #print(LKG,tk)
    ev = BIG.pop(0)
    time = ev["tm"]

    if ev["tp"] in [1,8]:
        printlog(ev)

    if ev["tp"] == 2:
        nk = tt(pa) 
        ev["data"] = (nk, N+nk)
        printlog(ev)
        for _ in range(nk):
            N = N + 1
            if LKG:
                rusis = tt(pr)
                trukme = round(np(ma[rusis-1], sa[rusis-1]))
                kirpejas = LKG.pop(0)
                tk[kirpejas-1] = tk[kirpejas-1] + trukme
                ev = {"tm": time+trukme, "tp": 5, "data": (N, kirpejas), "msg" : "Kl.{0} po apt.iseina.Kr.{1} laisvas.Aptarnauta:{2}"}
                BIG = pildoBIG(BIG,ev)
                info = (N, kirpejas, rusis, trukme, valmin(t+trukme), tk[kirpejas-1] )
                msg = "Kl.{0} pradejo apt. kr.{1} r={2} truk.={3} baigs:{4} atidirbs:{5}"
                ev = {"tm": time, "tp": 3, "data": info, "msg": msg}
            else:
                if len(ELG) < maxe:
                    ELG.append((t,N))
                    ev = {"tm": t, "tp": 4, "data": (N,len(ELG)), "msg": "Kl.{0} stojo i eile.Eiles ilgis tapo:{1}"}
                else:
                    info = (N,N-Napt-len(ELG)-maxkr+len(LKG))
                    msg = "Kl.{0} iseina be aptarnavimo.Neaptarnautu kl.skaicius:{1}"
                    ev = {"tm": t, "tp": 7, "data": info, "msg": msg}
            printlog(ev)
        t = time + int(round(ex(tau)))
        if t < pamilg:
            ev = {"tm": t, "tp": 2, "data": (0,0), "msg": "Atejo {0} kl.Bendras atejusiu skaicius:{1}."}
            BIG = pildoBIG(BIG, ev)

    if ev["tp"] == 5:
        Napt = Napt + 1
        ev["data"] = ev["data"] + (Napt,)
        printlog(ev)
        #LKG.append(ev["data"][1])
        LKG = pildoLKG(LKG,tk,ev["data"][1])
        if ELG:
            StLk, Nkl  = ELG.pop(0)
            Ns = Ns + 1
            Te = Te + (time - StLk)
            info = (Nkl, (time - StLk), len(ELG), Te, Ns)
            msg = "Kl.{0} isena is eil.Stovejo:{1} Eiles ilg.{2} Bendr.Stlk.{3} StSk.{4}"
            ev = {"tm": time, "tp": 6, "data": info, "msg": msg}
            printlog(ev)

            rusis = tt(pr)
            trukme = round(np(ma[rusis-1], sa[rusis-1]))
            kirpejas = LKG.pop(0)
            tk[kirpejas-1] = tk[kirpejas-1] + trukme
            ev = {"tm": time+trukme, "tp": 5, "data": (Nkl, kirpejas), "msg" : "Kl.{0} po apt.iseina.Kr.{1} laisvas.Aptarnauta:{2}"}
            BIG = pildoBIG(BIG,ev)
            info = (Nkl, kirpejas, rusis, trukme, valmin(time+trukme), tk[kirpejas-1] )
            msg = "Kl.{0} pradejo apt. kr.{1} r={2} truk.={3} baigs:{4} atidirbs:{5}"
            ev = {"tm": time, "tp": 3, "data": info, "msg": msg}
            printlog(ev)

